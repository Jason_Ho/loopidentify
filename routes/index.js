var express = require('express');
const nodemailer = require('nodemailer');
var router = express.Router();
var request = require('request')


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Sign up Official account'
  });
});


router.post('/', function (req, res, next) {
  let fields = req.body
  console.log(fields)

  let transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: 'dev@wattertek.com',
      pass: 'Wtk-1101'
    }
  });

  let options = {
    from: 'sys@wattertek.com', //寄件者
    to: 'jason.ho@wattertek.com', //收件者
    // cc: '', //副本
    //bcc: '', //密件副本
    subject: 'Office account creation : ' + fields.businessname, //主旨
    //嵌入 html 的內文
    html: `
            <h3>The following is the information of Official account creation :</h3>
            <h4>Loop ID : </h4>
            <p style="color:black;margin:20px;">  ${fields.loopid} </p>
            <h4>Business name : </h4>
            <p style="color:black;margin:20px;">  ${fields.businessname} </p>
            <h4>Business address : </h4>
            <p style="color:black;margin:20px;">  ${fields.businessaddress} </p>
            <h4>Content Person : </h4>
            <p style="color:black;margin:20px;">  ${fields.first} ${fields.last}  </p>
            <h4>Contact Email : </h4>
            <p style="color:black;margin:20px;">  ${fields.email} </p>
            <h4>Contact Phone : </h4>
            <p style="color:black;margin:20px;">  ${fields.mobile} </p>
            <h4>Package Feature : </h4>
            <p style="color:black;margin:20px;">  ${fields.Feature} </p>
            <br>
            <p></p>
            <h5>Regards</h5>
            `
  };
  return new Promise((resolve, reject) => {
    transporter.sendMail(options, function (err, info) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        console.log('Send mail finished');
        res.render('submitend')
        resolve();
      }
    });

  })





});
module.exports = router;